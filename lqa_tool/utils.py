#!/usr/bin/env python
###################################################################################
# Utilities for the LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from __future__ import print_function

import copy

def merge_profiles(original, update):
    """Merge two hashes. This is used to merge profiles."""

    def _merge_profiles(original, update):
        for k, v in update.items():
            if type(v) == dict and (k in original and type(original[k]) == dict):
                _merge_profiles(original[k], update[k])
            else:
                original[k] = update[k]
        return original

    original_copy = copy.deepcopy(original)
    return _merge_profiles(original_copy, update)

# Convenient callback functions that can be used by the wait command.
def print_add_msg(q, job):
    """Convenient function to print message after adding job to the queue."""
    print("({}) <= Job added to wait queue | {}: {}" \
        .format(len(q), job.status, job))

def print_wait_msg(q, job):
    """Convenient function to print message after polling job."""
    print("({}) Waiting job | {}: {}".format(len(q), job.status, job))

def print_remove_msg(q, job):
    """Convenient function to print message after removing job from the queue."""
    print("({}) => Job done | {}: {}".format(len(q), job.status, job))

def print_timeout_msg(q, timeout):
    """Convenient function to print message after timeout has been reached."""
    for job in q:
        print("({}) !! Wait timeout of {}s reached for job | {}: {}" \
            .format(len(q), timeout, job.status, job))

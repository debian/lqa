###################################################################################
# LAVA QA API - Exit codes
# Copyright (C) 2015, 2016 Collabora Ltd.

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

# Everything went well
SUCCESS=0

# A failure was reported by an operation (e.g a failed test was reported)
OPERATION_FAILED=1

# The application failed with an error
APPLICATION_ERROR=2

# These codes are used to determinate command exit code depending on job status.
EXIT_CODE_CANCELED_JOB   = -1
EXIT_CODE_INCOMPLETE_JOB = -2
